import org.junit.Test;
import tasks.*;

import static org.junit.Assert.assertEquals;

public class TestTasks {

    @Test
    public void testTask1() throws Exception {

        Task1 uniqueSequence1 = new Task1(new int[]{1, 2, 3, 4, 5});
        assertEquals(5, uniqueSequence1.getMaxUniqueSequenceLength());
        assertEquals(0, uniqueSequence1.getStartIndex());
        assertEquals(4, uniqueSequence1.getEndIndex());

        Task1 uniqueSequence2 = new Task1(new int[]{1, 2, 3, 1, 5});
        assertEquals(4, uniqueSequence2.getMaxUniqueSequenceLength());
        assertEquals(1, uniqueSequence2.getStartIndex());
        assertEquals(4, uniqueSequence2.getEndIndex());

    }

    @Test
    public void testTask2() throws Exception {
        assertEquals("test. for text Some", Task2.reverseWords("Some text for test."));
    }
}
