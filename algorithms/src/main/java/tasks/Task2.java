package tasks;


public class Task2 {

    private static final String SPACE = "\\s";

    public static String reverseWords(String text) {
        StringBuilder stringBuilder = new StringBuilder();

        for(String word : text.split(SPACE)) {
            stringBuilder.insert(0, word + " ");
        }

        return stringBuilder.toString().trim();
    }
}
