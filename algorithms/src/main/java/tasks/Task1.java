package tasks;


import java.util.Arrays;

public class Task1 {
    private int maxUniqueSequenceLength = 1;
    private int startIndex = 0;
    private int endIndex = 0;

    public Task1(int[] sequence) {
        if(canHandle(sequence)) {
            findMaxUniqueSequence(sequence);
        } else {
            throw new RuntimeException("Invalid sequence: " + Arrays.toString(sequence));
        }
    }

    private void findMaxUniqueSequence(int[] sequence) {
        for (int current = 1; current < sequence.length; current++) {
            int uniqueSequenceLength = 1;
            for (int previous = current - 1; previous >= startIndex; previous--) {
                if (sequence[current] != sequence[previous]) {
                    uniqueSequenceLength++;
                } else {
                    startIndex = previous + 1;
                    break;
                }
            }

            if(uniqueSequenceLength > maxUniqueSequenceLength) {
                maxUniqueSequenceLength = uniqueSequenceLength;
            }
        }

        endIndex = maxUniqueSequenceLength + startIndex - 1;
    }

    private boolean canHandle(int[] sequence) {
        return sequence != null && sequence.length != 0;
    }

    public int getMaxUniqueSequenceLength() {
        return maxUniqueSequenceLength;
    }

    public void setMaxUniqueSequenceLength(int maxUniqueSequenceLength) {
        this.maxUniqueSequenceLength = maxUniqueSequenceLength;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    @Override
    public String toString() {
        return "Task1{" +
                "endIndex=" + endIndex +
                ", startIndex=" + startIndex +
                ", maxUniqueSequenceLength=" + maxUniqueSequenceLength +
                '}';
    }
}
